Information
====
<br>This is sample application for af-steering-wheel-binding.
<br>At above of the screen will display usb-camera(UVC camera) video.
<br>At bottom of this screen will display the can information.
<br>Right now it can get VehicleSpeed,EngineSpeed,TransmissionMode information from af-steering-wheel-binding.
<br>Or you can change to use low-level-can-servcie.

* Hardware: Renesas m3ulcb
* Software: Daring Dab 4.0.0
* Application name: als-meter-demo
* Test Camera Device: Logitech c920r

How to compile and install
====
<br>	These is a sample recipe for als-meter-demo, you can just add that recipes into your project and bitbake.
<br>	Sample Recipes: als-meter-demo_git.bb

How to use
====
<br>1, If the camera has been connected, you can select the camera corresponding to the device ID, and set FPS ,
<br>pixel parameters, open ON/OFF switch on the right, you can see the camera display content.
<br>2, operation steering-wheel device corresponding function, you can see vehicle speed, engine speed, transmission mode changes.

Kernel configure
====
<br>You need to enable some kernel configure to enable usb camera.
* CONFIG_VIDEOBUF2_VMALLOC=y
* CONFIG_MEDIA_USB_SUPPORT=y
* CONFIG_USB_VIDEO_CLASS=y
* CONFIG_USB_VIDEO_CLASS_INPUT_EVDEV=y