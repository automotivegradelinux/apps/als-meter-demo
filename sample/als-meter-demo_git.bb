SUMMARY     = "Demo app for streeting-wheel"
DESCRIPTION = "AGL demo app for streeting-wheel"
HOMEPAGE    = "https://gerrit.automotivelinux.org/gerrit/#/admin/projects/apps/als-meter-demo"
SECTION     = "apps"

LICENSE     = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=ae6497158920d9524cf208c09cc4c984"

SRC_URI = "git://gerrit.automotivelinux.org/gerrit/apps/als-meter-demo;protocol=https;branch=master"
SRCREV  = "9457d3ebf0fd63f63f5d8abdd570868c0298a92f"

PV = "1.0+git${SRCPV}"
S  = "${WORKDIR}/git"

# build-time dependencies
DEPENDS += "qtquickcontrols2 opencv"

# runtime dependencies
RDEPENDS_${PN} += "agl-service-steering-wheel"

inherit qmake5 aglwgt
