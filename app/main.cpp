/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 * Copyright (C) 2016 The Qt Company Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtCore/QDebug>
#include <QtCore/QCommandLineParser>
#include <QtCore/QUrlQuery>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQuickControls2/QQuickStyle>

#include <QtQuick/QQuickWindow>
#include <qlibhomescreen.h>
#include <qlibwindowmanager.h>

#include "camera.h"

int main(int argc, char *argv[])
{
    QString myname = QString("alsmeterdemo");

    QGuiApplication app(argc, argv);
    app.setApplicationName(myname);
    app.setApplicationVersion(QStringLiteral("0.1.0"));
    app.setOrganizationDomain(QStringLiteral("automotivelinux.org"));
    app.setOrganizationName(QStringLiteral("AutomotiveGradeLinux"));

    QQuickStyle::setStyle("AGL");

    QCommandLineParser parser;
    parser.addPositionalArgument("port", app.translate("main", "port for binding"));
    parser.addPositionalArgument("secret", app.translate("main", "secret for binding"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(app);
    QStringList positionalArguments = parser.positionalArguments();

    qmlRegisterType<Camera>("Camera", 1, 0, "Camera");

    QQmlApplicationEngine engine;
    int port = 0;
    if (positionalArguments.length() == 2) {
        port = positionalArguments.takeFirst().toInt();
    }
    QString secret = positionalArguments.takeFirst();
    QUrl bindingAddressWS;
    bindingAddressWS.setScheme(QStringLiteral("ws"));
    bindingAddressWS.setHost(QStringLiteral("localhost"));
    bindingAddressWS.setPort(port);
    bindingAddressWS.setPath(QStringLiteral("/api"));
    QUrlQuery query;
    query.addQueryItem(QStringLiteral("token"), secret);
    bindingAddressWS.setQuery(query);
    QQmlContext *context = engine.rootContext();
    context->setContextProperty(QStringLiteral("bindingAddressWS"), bindingAddressWS);

    std::string token = secret.toStdString();
    QLibWindowmanager* qwm = new QLibWindowmanager();

    // WindowManager
    if(qwm->init(port,secret) != 0){
        exit(EXIT_FAILURE);
    }
    // Request a surface as described in layers.json windowmanager’s file
    if (qwm->requestSurface(myname) != 0) {
        exit(EXIT_FAILURE);
    }
    // Create an event callback against an event type. Here a lambda is called when SyncDraw event occurs
    qwm->set_event_handler(QLibWindowmanager::Event_SyncDraw, [qwm, myname](json_object *object) {
        fprintf(stderr, "Surface got syncDraw!\n");
        qwm->endDraw(myname);
    });

    // HomeScreen
    QLibHomeScreen* qhs = new QLibHomeScreen();
    qhs->init(port, token.c_str());
    // Set the event handler for Event_TapShortcut which will activate the surface for windowmanager
    qhs->set_event_handler(QLibHomeScreen::Event_TapShortcut, [qwm, myname](json_object *object){
        qDebug("Surface %s got tapShortcut\n", qPrintable(myname));
        qwm->activateSurface(myname);
    });
    AGLScreenInfo screenInfo(qwm->get_scale_factor());
    engine.rootContext()->setContextProperty(QStringLiteral("screenInfo"), &screenInfo);

    engine.load(QUrl(QStringLiteral("qrc:/Als-meter-demo.qml")));

    QObject *root = engine.rootObjects().first();
    QQuickWindow *window = qobject_cast<QQuickWindow *>(root);
    QObject::connect(window, SIGNAL(frameSwapped()), qwm, SLOT(slotActivateSurface()
    ));

    return app.exec();
}
