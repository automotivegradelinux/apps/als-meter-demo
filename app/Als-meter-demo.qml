/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import QtQuick 2.6
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtQuick.Window 2.2
import Camera 1.0
import QtWebSockets 1.0

ApplicationWindow {
    id: root
    width: 1080
    height: 1487

    property string als_meter_demo_request_str: ""
    property string api_str: "low-can"
    property var msgid_enu: { "call":2, "retok":3, "reterr":4, "event":5 }
    property int recirc:0
    property int vehiclespeed: 0
    property real enginespeed: 0
    property string wshift: "N"
    property int circulation: 0

    WebSocket {
        id: websocket
        url: bindingAddressWS

        onStatusChanged: {
            if (websocket.status === WebSocket.Error){
                console.log ("Error: " + websocket.errorString)
                websocket.active = false
                countdown.start()
            }else if (websocket.status === WebSocket.Open){
                console.log ("Socket Open")
                do_subscribe()
            }else if (websocket.status === WebSocket.Closed){
                console.log ("Socket closed")
            }
        }

        onTextMessageReceived: {
            var message_json = JSON.parse(message);
            console.log("Raw response: " + message)

            if (message_json[0] === msgid_enu.event){
                var propertyName = message_json[2].event.split("/")[1]
                if (propertyName === "messages.transmission.mode"){
                    wshift = message_json[2].data.value
                }else if (propertyName === "messages.vehicle.average.speed"){
                    vehiclespeed = message_json[2].data.value
                }else if (propertyName === "messages.engine.speed"){
                    enginespeed = message_json[2].data.value
                }
                else if ( message.indexOf("messages.hvac.circulation") > 0 )
                {
                    circulation = message_json[2].data.value ? 1 : 0
                }
            }else if (message_json[0] === msgid_enu.retok){
                console.log ("Response is OK!")
            }else{
                console.log ("Event type error:", message_json[0])
            }
        }

        active: false
    }

    Timer {
        id: countdown
        repeat: false
        interval: 3000
        triggeredOnStart: false
        onTriggered: {
            websocket.active = true
        }
    }

    Item {
        id: container
        anchors.centerIn: parent
        width: 1080
        height: 1487
        scale: screenInfo.scale_factor()

        Item {
            id: topscreen_up
            width: root.width
            height: (root.height - bottomscreen.height)/2

            CameraControl {
                id:cameracontrol_up
                listWH: true
                device: camera_up
                width: root.width
                height:80
            }
            Camera {
                id: camera_up
                width: root.width
                height: topscreen_up.height - cameracontrol_up.height
                anchors.top: cameracontrol_up.bottom
                onIsrunningChanged: {
                    camerabg_up.visible = !isrunning

                    if (isrunning){
                        if (cameracontrol_dn.device.cameraCnt() === 1)
                            cameracontrol_dn.switchstatus = false;
                    }else{
                        cameracontrol_dn.switchstatus = true;
                    }
                }
                onCamraCntChanged: {
                    if (!camera_dn.isrunning)
                        cameracontrol_dn.number = camera_dn.camranum();

                    if (!camera_up.isrunning)
                        cameracontrol_up.number = camera_up.camranum();
                }

                Image {
                    id: camerabg_up
                    anchors.centerIn: parent
                    width: 200
                    height: 200
                    source: "images/camera/camera_bg.svg"
                }
            }
        }

        Item {
            id: topscreen_dn
            width: root.width
            height: (root.height - bottomscreen.height)/2
            anchors.top: topscreen_up.bottom
            CameraControl {
                id:cameracontrol_dn
                listWH: false
                device: camera_dn
                width: root.width
                height:80
                back:0
            }
            Camera {
                id: camera_dn
                width: root.width
                height: topscreen_dn.height - cameracontrol_dn.height
                anchors.top: cameracontrol_dn.bottom
                onIsrunningChanged: {
                    camerabg_dn.visible = !isrunning

                    if (isrunning){
                        if (cameracontrol_up.device.cameraCnt() === 1)
                            cameracontrol_up.switchstatus = false;
                    }else{
                        cameracontrol_up.switchstatus = true;
                    }
                }

                onCamraCntChanged: {
                    if (!camera_up.isrunning)
                        cameracontrol_up.number = camera_up.camranum();

                    if (!camera_dn.isrunning)
                        cameracontrol_dn.number = camera_dn.camranum();
                }

                Image {
                    id: camerabg_dn
                    anchors.centerIn: parent
                    width: 200
                    height: 200
                    source: "images/camera/camera_bg.svg"
                }
            }
        }

        Item {
            id: bottomscreen
            width: root.width
            height: 600
            anchors.bottom: parent.bottom
            Image {
                width: parent.width
                height: parent.height
                asynchronous: true
                fillMode: Image.TileHorizontally
                smooth: true
                source: "images/homescreen/homebg_bottom.svg"
            }
            RowLayout {
                id: smtparts
                anchors.left: parent.left
                anchors.right: parent.right
                Speed {
                    id: speedparts
                    anchors.left: parent.left
                    anchors.leftMargin: 30
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 120
                    width: imgwidth
                    height: imgheight
                }
                ColumnLayout {
                    id: tachoparts
                    anchors.horizontalCenter: parent.horizontalCenter
                    Tacho {
                        anchors.top: parent.top
                        anchors.topMargin: -60
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: imgwidth
                        height: imgheight
                    }
                    MidOther {
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 110
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: imgwidth
                        height: imgheight
                    }
                }
                Mid {
                    id: midparts
                    anchors.right: parent.right
                    anchors.rightMargin: 25
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 90
                    width: imgwidth
                    height: imgheight
                }
            }
        }
    }
    onVisibleChanged: {
        if (visible){
            if (!websocket.active){
                websocket.active = true
            }else{
                do_subscribe()
            }
        }
        else {
            countdown.stop()
            if (websocket.active)
                do_unsubscribe()
        }
    }

    function do_call(binding, verb, event_name) {
        als_meter_demo_request_str = '[' + msgid_enu.call + ',"99999","' + binding+'/'+verb + '", {"event":"' + event_name + '"} ]'
        websocket.sendTextMessage (als_meter_demo_request_str)
    }

    function do_subscribe() {
        do_call(api_str, "subscribe", "vehicle.average.speed")
        do_call(api_str, "subscribe", "transmission.mode")
        do_call(api_str, "subscribe", "engine.speed")
        do_call(api_str, "subscribe", "hvac.circulation")
    }

    function do_unsubscribe() {
        do_call(api_str, "unsubscribe", "vehicle.average.speed")
        do_call(api_str, "unsubscribe", "transmission.mode")
        do_call(api_str, "unsubscribe", "engine.speed")
        do_call(api_str, "unsubscribe", "hvac.circulation")
    }
}
