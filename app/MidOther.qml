/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtWebSockets 1.0

Item {
    property real imgwidth: 430*120/150
    property real imgheight: 120

    property var recirc_mid: ["images/mid/recirc_outer.svg", "images/mid/recirc_inter.svg"]

    Image {
        width: imgwidth
        height: imgheight
        fillMode: Image.PreserveAspectFit
        asynchronous: true
        smooth: true
        source: recirc_mid[circulation]
    }
}
