TARGET = alsmeterdemo
QT = quickcontrols2

LIBS += -lopencv_core -lopencv_videoio

equals(QT_ARCH, "arm") {
    QMAKE_CXXFLAGS += -mfp16-format=ieee
}

HEADERS += \
    camera.h

SOURCES += \
    main.cpp \
    camera.cpp

CONFIG += link_pkgconfig
PKGCONFIG += qlibwindowmanager qlibhomescreen

RESOURCES += \
    als-meter-demo.qrc \
    images/images.qrc

include(app.pri)
